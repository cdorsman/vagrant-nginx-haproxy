# vagrant-nginx-haproxy
This is a package to setup two NGINX webservers and a HAProxy loadbalancer on MacOS.

## Requirements
You need to have a recent version of MacOS and two things installed:
+ vagrant 
+ ansible

## Make it all work
Just do a 'git pull', 'vagrant up' to start it all up and you're good to go.
 